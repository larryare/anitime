'use client';
import Link from 'next/link';
import { useEffect, useMemo, useRef, useState } from 'react';

function useRefreshableState(initialValue: any) {
  const [state, setState] = useState(0);
  const ref = useRef(state);

  useEffect(() => {
    function updateState() {
      ref.current++;
      setState(ref.current);
    }

    // Schedule state updates every 10 milliseconds
    const intervalId = setInterval(updateState, 20);

    return () => clearInterval(intervalId);
  }, [state]);

  return useMemo(() => ref.current, []);
}

function Anidate(ani: Date = new Date(2024, 4, 26)) {
  // const ani: Date = new Date(2023, 10, 19);
  let now = Date.now();
  let duration = now - ani.getTime();
  let years = duration / 31556900000;
  let majorMinor = years.toFixed(9);
  return {
    "ms": majorMinor,
    "seconds": (duration / 1000).toFixed(3),
    "minutes": (duration / 60000).toFixed(2),
    "hours": (duration / 3600000).toFixed(2),
    "days": (duration / 86400000).toFixed(4),
    "weeks": (duration / 604800000).toFixed(5),
    "months": (duration / 2629746000).toFixed(6),
  };
};

function NumDisplay(val: string, deli: string, min: boolean) {
  let vals = val.split(".").map((x) => { return Number(x) });
  // console.log(vals);
  if (min) {
    let what = vals[1];
    vals[1] = what % 60;
  }
  let nf = new Intl.NumberFormat();

  return (

    <p className='py-2'>
      <span suppressHydrationWarning className=''>
        {nf.format(vals[0])}
      </span>
      <span suppressHydrationWarning className='px-1 text-base sm:text-xl -top-6 align-super'>
        {vals[1].toString().padStart(3, '0')}
      </span>
      <span className='text-base'>{deli}</span>
    </p>
  )
}

export default function Home(params: any) {
  let dt = params["searchParams"].date ? params["searchParams"].date.split("-") : [2024, 4, 26];
  const dat = new Date(dt[0], dt[1] - 1, dt[2] - 1)
  const refresh = useRefreshableState(0);
  const anidate = Anidate(dat);
  // console.log(anidate.months.split('.')[0]);
  const full = anidate.ms.split(".")[0] + "y " + anidate.months.split(".")[0] + "mo " + anidate.weeks.split(".")[0] + "w " + anidate.days.split(".")[0] + "d " + anidate.hours.split(".")[0] + "h " + anidate.minutes.split(".")[0] + "m " + anidate.seconds.split(".")[0] + "s";
  return (
    <main className="flex h-screen w-screen flex-col items-center justify-between py-48 sm:text-6xl text-5xl font-bold">
      <div className='text-transparent bg-gradient-to-br from-red-800 to-blue-500 bg-clip-text flex items-center flex-col'>
        {NumDisplay(anidate.ms, "y", false)}
        {NumDisplay(anidate.months, "mo", false)}
        {NumDisplay(anidate.weeks, "w", false)}
        {NumDisplay(anidate.days, "d", false)}
        {NumDisplay(anidate.minutes, "m", true)}
        {NumDisplay(anidate.seconds, "s", false)}
      </div>
      <div className='text-center absolute bottom-2 text-xs sm:text-sm'>
        <p suppressHydrationWarning className='my-2 text-wrap  text-gray-400'>{full}</p>
        <Link href="https://kerezius.lt">
          <p className='text-slate-500'>made with ❤️ and ☕ by
            <span className='text-transparent bg-gradient-to-r from-green-500 to-yellow-300 bg-clip-text'> Lauris</span>
          </p>
        </Link>
      </div>
    </main>
  )
}
