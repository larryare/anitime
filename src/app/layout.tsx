import type { Metadata } from 'next'
import { Azeret_Mono, Inter, Rubik_Mono_One } from 'next/font/google'
import './globals.css'

const inter = Azeret_Mono({ subsets: ['latin'], weight: "variable" })

export const metadata: Metadata = {
  title: 'Anitime',
  description: 'Tiesiog lol',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  )
}
